package com.cyxteratest.aplicationtestcyxtera;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.cyxtera.middlware.MODEL.UserObject;
import com.cyxtera.middlware.NETWORK.Bl.GetUser;
import com.cyxtera.middlware.NETWORK.Bl.SetUser;
import com.cyxteratest.aplicationtestcyxtera.ModelTest.TestCaseUserObject;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.cyxteratest.aplicationtestcyxtera", appContext.getPackageName());
    }
    @Test
    public void test1CreateUser(){

        final TestCaseUserObject testCaseUserObject = new TestCaseUserObject();
        testCaseUserObject.setComplete(false);
        UserObject userObjectResult = new UserObject();
        UserObject userObject = new UserObject("", "Prueba Uno","persona20@mail.com", "prueba1", "", "prueba", null, true);

        new SetUser().createUserKeyForUidTest(userObject, new SetUser.OnGetUser() {
            @Override
            public void onGetUser(UserObject userObject) {
                testCaseUserObject.setUserObject(userObject);
                testCaseUserObject.setComplete(true);
            }
        });

        while(!testCaseUserObject.isComplete()){

        }

        assertEquals(userObject.getUid(), testCaseUserObject.getUserObject().getUid());
    }

    List<UserObject> userObjectsInit = null;
    List<UserObject> userObjectsFinal = null;
    @Test
    public void test1NoRepeatUser(){
        final TestCaseUserObject testCaseUserObject = new TestCaseUserObject();
        testCaseUserObject.setComplete(false);

        final UserObject userObject = new UserObject("", "Prueba Uno","persona20@mail.com", "prueba1", "", "prueba", null, true);

        new GetUser().GetListUsersWithRolUserTest(new GetUser.OnGetUsers() {
            @Override
            public void onGetListUsers(List<UserObject> userObjects) {
                userObjectsInit = userObjects;
                new SetUser().createUserKeyForUidTest(userObject, new SetUser.OnGetUser() {
                    @Override
                    public void onGetUser(UserObject userObject) {
                        new GetUser().GetListUsersWithRolUserTest(new GetUser.OnGetUsers() {
                            @Override
                            public void onGetListUsers(List<UserObject> userObjects) {
                                userObjectsFinal = userObjects;
                                testCaseUserObject.setComplete(true);
                            }
                        });

                    }
                });
            }
        });


        while(!testCaseUserObject.isComplete()){

        }

        assertEquals(userObjectsInit.size(), userObjectsFinal.size());
    }
}
