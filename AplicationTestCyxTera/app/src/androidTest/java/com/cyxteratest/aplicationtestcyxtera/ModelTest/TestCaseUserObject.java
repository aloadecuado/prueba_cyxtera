package com.cyxteratest.aplicationtestcyxtera.ModelTest;

import android.widget.ListView;

import com.cyxtera.middlware.MODEL.UserObject;

import java.util.List;

public class TestCaseUserObject {

    private UserObject userObject = null;

    public UserObject getUserObject() {
        return userObject;
    }

    public void setUserObject(UserObject userObject) {
        this.userObject = userObject;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    private boolean isComplete = false;

}
