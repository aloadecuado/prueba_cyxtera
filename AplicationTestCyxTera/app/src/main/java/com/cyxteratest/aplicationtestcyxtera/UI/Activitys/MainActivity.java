package com.cyxteratest.aplicationtestcyxtera.UI.Activitys;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.QuickContactBadge;

import com.cyxtera.middlware.MODEL.UserObject;
import com.cyxtera.middlware.NETWORK.Bl.SetUser;
import com.cyxteratest.aplicationtestcyxtera.R;
import com.cyxteratest.aplicationtestcyxtera.UI.Adapters.TimeZoneAdapter;
import com.firebase.ui.auth.AuthUI;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton.OnVisibilityChangedListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;


import static com.cyxteratest.aplicationtestcyxtera.UTIL.GlobalVar.USER_OBJECT;

public class MainActivity extends AppCompatActivity implements LocationListener {
    private static final int RC_SIGN_IN = 1032;

    protected LocationManager locationManager;
    private int REQUEST_PERMISSOS = 2390;
    String[] permissions = new String[2];
    double Lat = 0.0;
    double Lng = 0.0;

    ListView lvTimeZone;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvTimeZone = findViewById(R.id.lvTimeZone);
        FloatingActionButton btSignOut = findViewById(R.id.btSignOut);

        btSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                auth.signOut();

                startActivity(new Intent(MainActivity.this, MainActivity.class));
                MainActivity.this.finish();
            }
        });
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            permissions[0] = Manifest.permission.ACCESS_FINE_LOCATION;
            permissions[1] = Manifest.permission.ACCESS_COARSE_LOCATION;


            requestPermissions(permissions, REQUEST_PERMISSOS);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        FirebaseApp.initializeApp(this);

        FirebaseAuth auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() != null) {
            // already signed in
            floidNavigation(auth);
        } else {

            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setIsSmartLockEnabled(false)
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.EmailBuilder().build()))
                            .build(),
                    RC_SIGN_IN);
            // not signed in
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){

        if(requestCode == REQUEST_PERMISSOS){
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                permissions[0] = Manifest.permission.ACCESS_FINE_LOCATION;
                permissions[1] = Manifest.permission.ACCESS_COARSE_LOCATION;


                requestPermissions(permissions, REQUEST_PERMISSOS);
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            // Successfully signed in
            if (resultCode == RESULT_OK) {

                final FirebaseAuth auth = FirebaseAuth.getInstance();
                floidNavigation(auth);
                return;
            }

        }



    }

    private void floidNavigation(final FirebaseAuth auth) {
        FirebaseUser user = auth.getCurrentUser();

        UserObject userObject;

        if(user.getPhotoUrl() != null){
            userObject = new UserObject("",user.getDisplayName(), user.getEmail(), user.getUid(), user.getPhotoUrl().getUserInfo(),"use",null, true);

        }else{
            userObject = new UserObject("",user.getDisplayName(), user.getEmail(), user.getUid(), "https://www.cyxtera.com/images/Cyxtera_RGB_1200x800_Primary_White-FACEBOOKog.jpg","use", null, true);
        }

        new SetUser().createUserKeyForUid(MainActivity.this,userObject, new SetUser.OnGetUser() {
            @Override
            public void onGetUser(UserObject userObject1) {

                USER_OBJECT = userObject1;

                new SetUser().addAutenticationTimeZone(MainActivity.this, USER_OBJECT, Lat, Lng, new SetUser.OnGetUser() {
                    @Override
                    public void onGetUser(UserObject userObject2) {
                        USER_OBJECT = userObject2;
                        reloadLitView();
                    }
                });


            }
        });
    }

    private void reloadLitView(){

        TimeZoneAdapter timeZoneAdapter = new TimeZoneAdapter(this, R.layout.adapter_time_zone, USER_OBJECT.getTimeZoneObjects());
        lvTimeZone.setAdapter(timeZoneAdapter);
    }
    @Override
    public void onLocationChanged(Location location) {

        Lat = location.getLatitude();
        Lng = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
