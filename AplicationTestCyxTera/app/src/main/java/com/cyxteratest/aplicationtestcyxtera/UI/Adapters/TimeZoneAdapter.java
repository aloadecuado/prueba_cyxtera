package com.cyxteratest.aplicationtestcyxtera.UI.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import com.cyxtera.middlware.MODEL.TimeZoneObject;
import com.cyxteratest.aplicationtestcyxtera.R;

import java.util.List;

public class TimeZoneAdapter extends ArrayAdapter<TimeZoneObject> {

    public TimeZoneAdapter(@NonNull Context mcontext, @LayoutRes int resource, List<TimeZoneObject> creditCardObjects)
    {
        super(mcontext,resource, creditCardObjects);

    }




    @Override
    public View getView(int groupPosition, View convertView, ViewGroup parent) {

        Context con = null;
        View view = convertView;
        if (view == null)
        {
            //  LayoutInflater vi = (LayoutInflater)con.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LayoutInflater vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.adapter_time_zone, null);
        }


        TextView tvZone = (TextView) view.findViewById(R.id.tvZone);
        TextView tvContry = (TextView) view.findViewById(R.id.tvContry);
        TextView tvDate = (TextView) view.findViewById(R.id.tvDate);


        TimeZoneObject timeZoneObject = getItem(groupPosition);

        tvZone.setText("zone: " + timeZoneObject.getTimezoneId());
        tvContry.setText("Contry: " +timeZoneObject.getCountryName());
        tvDate.setText("Date: " + timeZoneObject.getTime());


        return view;
    }
}
