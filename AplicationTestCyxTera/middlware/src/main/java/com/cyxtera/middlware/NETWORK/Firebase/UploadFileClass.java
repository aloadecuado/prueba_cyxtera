package com.cyxtera.middlware.NETWORK.Firebase;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

/**
 * Created by pedrodaza on 24/01/18.
 */

public class UploadFileClass {

    public interface OnSetDataFile
    {
        void OnSetFile(String UrlDownload);
        void OnSetError(String error);
    }

    FirebaseStorage storage = FirebaseStorage.getInstance();
    public void UploadFile(String Referencia, String NombreArchivo, byte[] Archivo, final OnSetDataFile listener)
    {
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

        // Create a reference to "mountains.jpg"
        StorageReference mountainsRef = storageRef.child(NombreArchivo);

        // Create a reference to 'images/mountains.jpg'
        StorageReference mountainImagesRef = storageRef.child(Referencia);

        mountainsRef.getName().equals(mountainImagesRef.getName());    // true
        mountainsRef.getPath().equals(mountainImagesRef.getPath());

        UploadTask uploadTask = mountainsRef.putBytes(Archivo);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {


                listener.OnSetError(exception.getLocalizedMessage());
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getUploadSessionUri();



                listener.OnSetFile(downloadUrl.toString());

            }
        });
    }
}
