package com.cyxtera.middlware.UTIL;

public interface GlobalVars {
    public static String KURLUser = "Users";
    public static String KURLRest = "http://api.geonames.org/";
    public static String KURLRestTimeZone = KURLRest + "timezoneJSON?formatted=true&lat={DeviceLatitude}&lng={DeviceLongitude}&username=qa_mobile_easy&style=full";
}
