package com.cyxtera.middlware.NETWORK.Bl;

import android.app.ProgressDialog;
import android.content.Context;

import com.cyxtera.middlware.MODEL.TimeZoneObject;
import com.cyxtera.middlware.MODEL.UserObject;
import com.cyxtera.middlware.NETWORK.Firebase.GetObjectClass;
import com.cyxtera.middlware.NETWORK.Firebase.SetObjectClass;
import com.cyxtera.middlware.UTIL.AlertsClass;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

import static com.cyxtera.middlware.UTIL.GlobalVars.KURLUser;


public class SetUser {
    public interface OnGetUser {
        void onGetUser(UserObject userObject);
    }
    public void createUserKeyForUid(final Context context, final UserObject userObject, final OnGetUser listener)
    {
        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(KURLUser, "uid", userObject.getUid(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {


                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    new AlertsClass().hideProggress(progressDialog);
                    UserObject userObject1 = snapshot1.getValue(UserObject.class);
                    userObject1.setId(snapshot1.getKey());
                    listener.onGetUser(userObject1);
                    break;
                }

            }

            @Override
            public void onGetNullObject() {
                new SetObjectClass().SetObjectKeyForAutoKey(userObject, KURLUser, new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {
                        new AlertsClass().hideProggress(progressDialog);
                        UserObject userObject1 = snapshot.getValue(UserObject.class);

                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);
                    }

                    @Override
                    public void onGetError(String Err) {
                        new AlertsClass().showAlerInfo(context, "Información", Err);
                        new AlertsClass().hideProggress(progressDialog);
                    }
                });
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, "Información", Err);
                new AlertsClass().hideProggress(progressDialog);

            }
        });
    }
    public void createUserKeyForUidTest(final UserObject userObject, final OnGetUser listener)
    {

        new GetObjectClass().getSimpleListObjectUidKeyWhithEqualsToString(KURLUser, "uid", userObject.getUid(), new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {


                for (DataSnapshot snapshot1 : snapshot.getChildren())
                {

                    UserObject userObject1 = snapshot1.getValue(UserObject.class);
                    userObject1.setId(snapshot1.getKey());
                    listener.onGetUser(userObject1);
                    break;
                }

            }

            @Override
            public void onGetNullObject() {
                new SetObjectClass().SetObjectKeyForAutoKey(userObject, KURLUser, new SetObjectClass.onGetObject() {
                    @Override
                    public void onGetObject(DataSnapshot snapshot) {

                        UserObject userObject1 = snapshot.getValue(UserObject.class);

                        userObject1.setId(snapshot.getKey());
                        listener.onGetUser(userObject1);
                    }

                    @Override
                    public void onGetError(String Err) {

                    }
                });
            }

            @Override
            public void OnGetError(String Err) {

            }
        });

    }

    public void setUserForId(UserObject userObject, final OnGetUser listener){
        new SetObjectClass().SetObjectKeyForCustomKey(userObject, KURLUser, userObject.getId(), new SetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot) {

                UserObject userObject1 = snapshot.getValue(UserObject.class);

                userObject1.setId(snapshot.getKey());
                listener.onGetUser(userObject1);
            }

            @Override
            public void onGetError(String Err) {

            }
        });

    }
    public void addAutenticationTimeZone(Context context,final UserObject userObject, double lat, double lng, final OnGetUser listener){

        new GetTimeZone().getTimeZone(context, lat, lng, new GetTimeZone.OnGetTimeZone() {
            @Override
            public void onGetTimeZone(TimeZoneObject timeZoneObject) {


                ArrayList zoneObjects = (ArrayList) userObject.getTimeZoneObjects();

                if (zoneObjects != null){
                    zoneObjects.add(timeZoneObject);
                }else{
                    zoneObjects = new ArrayList();
                    zoneObjects.add(timeZoneObject);
                }


                userObject.setTimeZoneObjects(zoneObjects);

                setUserForId(userObject, new OnGetUser() {
                    @Override
                    public void onGetUser(UserObject userObject1) {
                        listener.onGetUser(userObject1);
                    }
                });

            }
        });
    }


}
