package com.cyxtera.middlware.NETWORK.Bl;

import android.app.ProgressDialog;
import android.content.Context;

import com.cyxtera.middlware.MODEL.TimeZoneObject;
import com.cyxtera.middlware.NETWORK.Rest.GetRest;
import com.cyxtera.middlware.UTIL.AlertsClass;
import com.google.gson.Gson;

import static com.cyxtera.middlware.UTIL.GlobalVars.KURLRestTimeZone;

public class GetTimeZone {
    public interface OnGetTimeZone{
        void onGetTimeZone(TimeZoneObject timeZoneObject);
    }

    public void getTimeZone(final Context context, double lat, double lng, final OnGetTimeZone listener){
        final ProgressDialog progressDialog = new AlertsClass().showProggres(context);
        String url = KURLRestTimeZone.replace("{DeviceLatitude}", "" + lat).replace("{DeviceLongitude}", "" + lng);
        new GetRest().getStringForUrl(context, url, new GetRest.OnGetDataStirng() {
            @Override
            public void onGetDataString(String json) {
                new AlertsClass().hideProggress(progressDialog);
                TimeZoneObject timeZoneObject = new Gson().fromJson(json, TimeZoneObject.class);
                listener.onGetTimeZone(timeZoneObject);
            }

            @Override
            public void onGetError(String error) {
                new AlertsClass().showAlerInfo(context, "Información", error);
                new AlertsClass().hideProggress(progressDialog);
            }
        });
    }
}
