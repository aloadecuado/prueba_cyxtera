package com.cyxtera.middlware.MODEL;

import java.util.List;

public class UserObject {

    private String id = "";
    private String name = "";
    private String email = "";
    private String uid = "";
    private String urlPhoto = "";
    private String rol = "user";
    private List<TimeZoneObject> timeZoneObjects = null;


    public UserObject(String id, String name, String email, String uid, String urlPhoto, String rol, List<TimeZoneObject> timeZoneObjects, boolean isActive) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.uid = uid;
        this.urlPhoto = urlPhoto;
        this.rol = rol;
        this.timeZoneObjects = timeZoneObjects;
        this.isActive = isActive;
    }



    public List<TimeZoneObject> getTimeZoneObjects() {
        return timeZoneObjects;
    }

    public void setTimeZoneObjects(List<TimeZoneObject> timeZoneObjects) {
        this.timeZoneObjects = timeZoneObjects;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    private boolean isActive = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public UserObject() {

    }


}
