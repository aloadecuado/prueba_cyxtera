package com.cyxtera.middlware.NETWORK.Rest;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class GetRest {
    public interface OnGetDataStirng{
        void onGetDataString(String json);
        void onGetError(String error);
    }

    public void getStringForUrl(Context context, String url, final OnGetDataStirng listener){

        RequestQueue queue = Volley.newRequestQueue(context);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.d("Response", response.toString());
                        listener.onGetDataString(response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", "" + error.networkResponse.statusCode);
                        listener.onGetError("" + error.networkResponse.statusCode);
                    }
                }
        );

// add it to the RequestQueue
        queue.add(getRequest);
    }
}
