package com.cyxtera.middlware.MODEL;

public class TimeZoneObject {
    private String sunrise ="";
    private String countryCode ="";
    private String sunset ="";
    private String timezoneId ="";
    private String countryName ="";
    private String time ="";
    private double lng = 0.0;
    private double gmtOffset = 0.0;
    private double rawOffset = 0.0;
    private double dstOffset = 0.0;
    private double lat = 0.0;


    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getTimezoneId() {
        return timezoneId;
    }

    public void setTimezoneId(String timezoneId) {
        this.timezoneId = timezoneId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getGmtOffset() {
        return gmtOffset;
    }

    public void setGmtOffset(double gmtOffset) {
        this.gmtOffset = gmtOffset;
    }

    public double getRawOffset() {
        return rawOffset;
    }

    public void setRawOffset(double rawOffset) {
        this.rawOffset = rawOffset;
    }

    public double getDstOffset() {
        return dstOffset;
    }

    public void setDstOffset(double dstOffset) {
        this.dstOffset = dstOffset;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }


}
