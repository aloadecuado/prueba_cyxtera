package com.cyxtera.middlware.NETWORK.Bl;

import android.content.Context;

import com.cyxtera.middlware.MODEL.UserObject;
import com.cyxtera.middlware.NETWORK.Firebase.GetObjectClass;
import com.cyxtera.middlware.R;
import com.cyxtera.middlware.UTIL.AlertsClass;
import com.google.firebase.database.DataSnapshot;


import java.util.ArrayList;
import java.util.List;

import static com.cyxtera.middlware.UTIL.GlobalVars.KURLUser;


public class GetUser {
    public interface OnGetUser
    {
        void onGetUser(UserObject userObjects);
        void onGetUserNoExist();
    }

    public interface OnGetUsers
    {
        void onGetListUsers(List<UserObject> userObjects);
    }

    public void getUserForId(final Context context, String userId, final OnGetUser listener) {
        new GetObjectClass().getSimpleObjectsForKey(KURLUser, userId, new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                UserObject userObject = snapshot.getValue(UserObject.class);
                userObject.setId(snapshot.getKey());
                if(userObject != null)
                {
                    listener.onGetUser(userObject);
                }
                else
                {
                    new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_title_information));
                }
            }

            @Override
            public void onGetNullObject() {
                listener.onGetUserNoExist();
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), context.getResources().getString(R.string.alert_title_information));
            }

            @Override
            public void OnGetError(String Err) {
                new AlertsClass().showAlerInfo(context, context.getResources().getString(R.string.alert_title_information), Err);
            }
        });

    }

    public void GetListUsersWithRolUserTest(final OnGetUsers listener) {

        new GetObjectClass().getListenerListObjectEqualsString(KURLUser, "rol", "prueba", new GetObjectClass.onGetObject() {
            @Override
            public void onGetObject(DataSnapshot snapshot, String key) {
                ArrayList<UserObject> userObjects = new ArrayList<>();

                for(DataSnapshot snapshot1 : snapshot.getChildren())
                {
                    UserObject userObject = snapshot1.getValue(UserObject.class);
                    userObject.setId(snapshot1.getKey());
                    userObjects.add(userObject);
                }

                if (userObjects.size() >= 1)
                {
                    listener.onGetListUsers(userObjects);
                }
                else
                {
                    ArrayList<UserObject> userObjects1 = new ArrayList<>();
                    listener.onGetListUsers(userObjects1);
                }
            }

            @Override
            public void onGetNullObject() {
                ArrayList<UserObject> userObjects = new ArrayList<>();
                listener.onGetListUsers(userObjects);
            }

            @Override
            public void OnGetError(String Err) {

            }
        });
    }


}
